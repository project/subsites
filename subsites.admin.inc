<?php
/**
 * @file
 * Admin forms and hooks
 */

/**
 * Menu callback, show a list of all subsites.
 */
function subsites_admin_display_form($form, &$form_state) {
  $subsites = subsites_get_all();
  $themes = _subsites_theme_options();

  $form = array();
  $form['subsites'] = array('#tree' => TRUE);
  foreach ($subsites as $subsite) {
    $form['subsites'][$subsite['sid']] = array();
    $form['subsites'][$subsite['sid']]['sid'] = array(
      '#type' => 'value',
      '#value' => $subsite['sid'],
    );
    $form['subsites'][$subsite['sid']]['name'] = array(
      '#markup' => $subsite['name'],
    );
    $form['subsites'][$subsite['sid']]['status'] = array(
      '#type' => 'checkbox',
      '#default_value' => $subsite['status'],
    );
    $form['subsites'][$subsite['sid']]['theme'] = array(
      '#markup' => isset($themes[$subsite['theme']]) ? htmlentities($themes[$subsite['theme']]) : '',
    );
    $form['subsites'][$subsite['sid']]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $subsite['weight'],
    );
    $form['subsites'][$subsite['sid']]['edit'] = array(
      '#markup' => l(t('Edit'), 'admin/structure/subsites/' . $subsite['sid'] . '/edit'),
    );
    $form['subsites'][$subsite['sid']]['delete'] = array(
      '#markup' => l(t('Delete'), 'admin/structure/subsites/' . $subsite['sid'] . '/delete'),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Renders the admin_display_form
 */
function theme_subsites_admin_display_form($variables) {
  $form = $variables['form'];
  drupal_add_tabledrag('subsites-order', 'order', 'sibling', 'subsites-order-weight');

  $header = array(
    array('data' => t('Subsite')),
    array('data' => t('Enabled')),
    array('data' => t('Theme')),
    array('data' => t('Weight')),
    array(
      'data' => t('Operations'),
      'colspan' => '2',
    ),
  );

  $rows = array();
  foreach (element_children($form['subsites']) as $key => $id) {
    // Don't take form control structures.
    if (is_array($form['subsites'][$id]['name'])) {
      $form['subsites'][$id]['weight']['#attributes']['class'] = array('subsites-order-weight');
      $rows[] = array(
        'data' => array(
          drupal_render($form['subsites'][$id]['name']),
          drupal_render($form['subsites'][$id]['status']),
          drupal_render($form['subsites'][$id]['theme']),
          drupal_render($form['subsites'][$id]['weight']),
          drupal_render($form['subsites'][$id]['edit']),
          drupal_render($form['subsites'][$id]['delete']),
        ),
        'class' => array('draggable'),
      );
    }
  }
  if (empty($rows)) {
    $rows[] = array(array(
        'data' => t('No subsites available.'),
        'colspan' => '6',
      ));
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'subsites-order')));

  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Process subsites order configuration form submission.
 */
function subsites_admin_display_form_submit($form, &$form_state) {
  foreach ($form_state['values']['subsites'] as $subsite) {
    subsites_save(array(
      'sid' => $subsite['sid'],
      'status' => $subsite['status'],
      'weight' => $subsite['weight'],
    ));
  }

  drupal_set_message(t('Subsite configuration changes saved.'));
}

/**
 * Subsite information form
 */
function subsites_edit_form($form, $form_state, $subsite = NULL) {
  $form = array();

  if (isset($subsite['sid'])) {
    $form['sid'] = array(
      '#type' => 'value',
      '#value' => $subsite['sid'],
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => isset($subsite['name']) ? $subsite['name'] : '',
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($subsite['weight']) ? $subsite['weight'] : 0,
  );
  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => isset($subsite['status']) ? $subsite['status'] : NULL,
  );

  // Override Site information for this subsite
  $form['site_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site information'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['site_info']['site_name'] = array(
    '#title' => t('Site name'),
    '#type' => 'textfield',
    '#default_value' => isset($subsite['site_name']) ? $subsite['site_name'] : NULL,
  );

  $form['site_info']['slogan'] = array(
    '#title' => t('Slogan'),
    '#type' => 'textfield',
    '#description' => t('How this is used depends on your site\'s theme.'),
    '#default_value' => isset($subsite['slogan']) ? $subsite['slogan'] : NULL,
  );

  $form['site_info']['site_frontpage'] = array(
    '#title' => t('Default front page'),
    '#type' => 'textfield',
    '#default_value' => isset($subsite['site_frontpage']) ? drupal_get_path_alias($subsite['site_frontpage']) : NULL,
    '#size' => 40,
    '#description' => t('Optionally, specify a relative URL to display as the front page.  Leave blank to display the default content feed.'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
  );

  // Add buttons for Save and Delete
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#access' => !empty($subsite['sid']),
    '#submit' => array('subsites_delete_submit'),
  );

  return $form;
}

/**
 * Validate the subsites edit form.
 */
function subsites_edit_form_validate($form, &$form_state) {
  // Check for empty front page path.
  if (!empty($form_state['values']['site_frontpage'])) {
    // Get the normal path of the front page.
    form_set_value($form['site_info']['site_frontpage'], drupal_get_normal_path($form_state['values']['site_frontpage']), $form_state);
  }
  // Validate front page path.
  if (!drupal_valid_path($form_state['values']['site_frontpage'])) {
    form_set_error('site_frontpage', t("The path '%path' is either invalid or you do not have access to it.", array('%path' => $form_state['values']['site_frontpage'])));
  }
}

/**
 * Submit handler for the subsites_form form.
 */
function subsites_edit_form_submit($form, &$form_state) {
  $sid = isset($form_state['values']['sid']) ? $form_state['values']['sid'] : NULL;

  $subsite = array(
    'sid' => $sid,
    'name' => $form_state['values']['name'],
    'weight' => $form_state['values']['weight'],
    'status' => $form_state['values']['status'],
    'site_name' => $form_state['values']['site_name'],
    'slogan' => $form_state['values']['slogan'],
    'site_frontpage' => $form_state['values']['site_frontpage'],
  );
  $sid = subsites_save($subsite);

  // Allow subsequent #submit handlers to know what the subsite ID is
  $form_state['values']['sid'] = $sid;

  $form_state['redirect'] = 'admin/structure/subsites/' . $sid;
  drupal_set_message(t('Subsite configuration changes saved.'));
}

/**
 * Submit function for the 'Delete' button on the subsites editing form.
 */
function subsites_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/subsites/' . $form_state['values']['sid'] . '/delete';
}

/**
 * Displays a table of subsite nodes that are set to active this subsite.  Used by the subsites_content_nodes form.
 *
 * @param array $subsite
 *   Array of subsite meta data.
 */
function subsites_content_nodes($subsite) {
  $output = '';

  $header = array(t('Title'), t('Actions'));
  $result = db_query("SELECT n.nid, n.title FROM {subsites_node} sn INNER JOIN {node} n ON n.nid = sn.nid WHERE sid = :sid", array(':sid' => $subsite['sid']));
  $rows = array();
  foreach ($result as $row) {
    $rows[] = array(
      l($row->title, 'node/' . $row->nid),
      l(t('Remove from subsite'), 'admin/structure/subsites/' . $subsite['sid'] . '/content/nodes/delete/' . $row->nid),
    );
  }

  if (empty($rows)) {
    $output = t('This subsite has no nodes.');
  }
  else {
    $output .= theme('table', array('header' => $header, 'rows' => $rows));
  }

  return $output;
}

/**
 * Subsite nodes form, page callback for admin/structure/subsites/%subsites/content.  Show existing nodes set to
 * activate the subsite and allow users to add a new one.
 */
function subsites_content_nodes_form($form, &$form_state, $subsite) {
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $subsite['sid'],
  );
  $form['existing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Existing nodes'),
  );
  $form['existing']['nodes'] = array(
    '#markup' => subsites_content_nodes($subsite),
  );
  $form['add'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add a new node'),
    '#description' => t('Find a new node to add to this subsite.'),
  );
  $form['add']['nid'] = array(
    '#type' => 'textfield',
    '#autocomplete_path' => 'subsites/add-node',
  );
  $form['add']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

/**
 * Submit callback for subsite nodes form
 */
function subsites_content_nodes_form_submit($form, &$form_state) {
  $matches = array();
  preg_match('/\[nid:(\d+)\]/', $form_state['values']['nid'], $matches);
  if (isset($matches[1]) && is_numeric($matches[1])) {
    $nid = $matches[1];

    $exists = db_query("SELECT nid FROM {subsites_node} WHERE nid = :nid AND sid = :sid", array(':nid' => $nid, ':sid' => $form_state['values']['sid']))->fetchField();
    if (!$exists) {
      db_insert('subsites_node')
        ->fields(array(
          'nid' => $nid,
          'sid' => $form_state['values']['sid'],
        ))
        ->execute();
      drupal_set_message(t('Added node to subsite.'));
    }
    else {
      drupal_set_message(t('Node not added. Was already in subsite.'), 'error');
    }
  }
  else {
    drupal_set_message(t('Node not added. Not a valid node.'), 'error');
  }
}

/**
 * Autocomplete callback for subsite nodes
 */
function subsites_add_node($string) {
  $matches = array();
  $result = db_select('node')
    ->fields('node', array('title', 'nid'))
    ->condition('title', db_like($string) . '%', 'LIKE')
    ->range(0, 10)->execute();

  foreach ($result as $row) {
    $matches['[nid:' . $row->nid . '] ' . $row->title] = check_plain($row->title);
  }
  print drupal_json_encode($matches);
  exit();
}

/**
 * Menu callback -- Ask for confirmation of subsite deletion
 */
function subsites_content_nodes_delete_confirm_form($form, &$form_state, $subsite, $node) {
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $subsite['sid'],
  );
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %node from %subsite?', array('%node' => $node->title, '%subsite' => $subsite['name'])),
    'admin/structure/subsites/' . $subsite['sid'] . '/content/nodes',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for executing subsite deletion after confirmation
 */
function subsites_content_nodes_delete_confirm_form_submit($form, &$form_state) {
  db_delete('subsites_node')
    ->condition('sid', $form_state['values']['sid'])
    ->condition('nid', $form_state['values']['nid'])
    ->execute();

  $form_state['redirect'] = 'admin/structure/subsites/' . $form_state['values']['sid'] . '/content/nodes';

  drupal_set_message(t('Succesfully deleted node from subsite.'));
}

/**
 * Subsite menu selection form.
 */
function subsites_content_menu_form($form, $form_state, $subsite) {
  $form = array();

  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $subsite['sid'],
  );

  $menus = menu_get_menus();
  $subsite_menus = db_query('SELECT menu_name FROM {subsites_menu} WHERE sid = :sid', array(':sid' => $subsite['sid']))->fetchAllKeyed(0, 0);

  $form['menu_name'] = array(
    '#type' => 'select',
    '#title' => t('Menu(s)'),
    '#options' => $menus,
    '#multiple' => TRUE,
    '#default_value' => $subsite_menus,
    '#description' => t('Select the menus for which this subsite should be active.'),
    '#size' => 10,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit function for the subsites_content_menu_form.  Save the selected menus to the database.
 */
function subsites_content_menu_form_submit($form, &$form_state) {
  // Clear menus for this subsite, start a transaction in case we fail in the middle
  $transaction = db_transaction();
  try {
    // Remove entries
    db_delete('subsites_menu')
      ->condition('sid', $form_state['values']['sid'])
      ->execute();

    // Add selected menus for the sid
    if (!empty($form_state['values']['menu_name'])) {
      $query = db_insert('subsites_menu')
        ->fields(array('sid', 'menu_name'));

      foreach ($form_state['values']['menu_name'] as $menu_name) {
        $query->values(array('sid' => $form_state['values']['sid'], 'menu_name' => $menu_name));
      }
      $query->execute();
    }

    drupal_set_message(t('Subsite configuration changes saved.'));
  }
  catch(Exception $e) {
    $transaction->rollback();
    drupal_set_message('Failed to save seleted menus.  Error: ' . $e->getMessagea(), 'error');
  }
}

/**
 * Subsite content pages form
 */
function subsites_content_pages_form($form, $form_state, $subsite) {
  $form = array();

  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $subsite['sid'],
  );

  $access = user_access('use PHP for settings');
  if (isset($subsite['visibility']) && $subsite['visibility'] == SUBSITES_VISIBILITY_PHP && !$access) {
    $form['visibility'] = array(
      '#type' => 'value',
      '#value' => SUBSITES_VISIBILITY_PHP,
    );
    $form['pages'] = array(
      '#type' => 'value',
      '#value' => isset($subsite['pages']) ? $subsite['pages'] : '',
    );
  }
  else {
    $options = array(
      SUBSITES_VISIBILITY_NOTLISTED => t('All pages except those listed'),
      SUBSITES_VISIBILITY_LISTED => t('Only the listed pages'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if (module_exists('php') && $access) {
      $options += array(SUBSITES_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Activate subsite on specific pages'),
      '#options' => $options,
      '#default_value' => isset($subsite['visibility']) ? $subsite['visibility'] : SUBSITES_VISIBILITY_NOTLISTED,
    );
    $form['pages'] = array(
      '#type' => 'textarea',
      '#title' => '<span class="element-invisible">' . $title . '</span>',
      '#default_value' => isset($subsite['pages']) ? $subsite['pages'] : '',
      '#description' => $description,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the subsites_content_pages_form form.  Save values for pages and visibility.
 */
function subsites_content_pages_form_submit($form, &$form_state) {
  subsites_save(array(
    'sid' => $form_state['values']['sid'],
    'pages' => $form_state['values']['pages'],
    'visibility' => $form_state['values']['visibility'],
  ));

  $form_state['redirect'] = 'admin/structure/subsites/' . $form_state['values']['sid'] . '/content/pages';
  drupal_set_message(t('Subsite configuration changes saved.'));
}

/**
 * Subsite appearance / theme selection form
 */
function subsites_look_form($form, $form_state, $subsite) {
  $form = array();

  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $subsite['sid'],
  );

  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#options' => _subsites_theme_options(),
    '#default_value' => $subsite['theme'],
    '#description' => t('Choose a theme to activate when the subsite is active.'),
  );

  $css_path = subsites_get_css_path($subsite['sid']);
  $css = NULL;
  if (file_exists($css_path)) {
    $css = file_get_contents($css_path);
  }

  $form['css'] = array(
    '#type' => 'textarea',
    '#title' => t('CSS'),
    '#default_value' => $css,
  );

  if ($css_path) {
    $form['css']['#description'] = t('You can also alter this CSS code by editing the file !css_path', array('!css_path' => $css_path));
  }

  $form['logo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logo image settings'),
    '#description' => t('Optionally override the logo when this subsite is active.'),
    '#attributes' => array('class' => array('theme-settings-bottom')),
  );

  $form['logo']['default_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the theme logo'),
    '#default_value' => isset($subsite['default_logo']) ? $subsite['default_logo'] : 1,
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the logo configured for it.')
  );

  $form['logo']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the logo settings when using the default logo.
      'invisible' => array(
        'input[name="default_logo"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['logo']['settings']['logo_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom logo'),
    '#description' => t('The path to the file you would like to use as your logo file instead of the default logo.'),
    '#default_value' => isset($subsite['logo_path']) ? $subsite['logo_path'] : '',
  );

  // Friendly path
  if (file_uri_scheme($subsite['logo_path']) == 'public') {
    $friendly_path = file_uri_target($subsite['logo_path']);
    $form['logo']['settings']['logo_path']['#default_value'] = $friendly_path;
  }

  $form['logo']['settings']['logo_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload logo image'),
    '#maxlength' => 40,
    '#description' => t("If you don't have direct file access to the server, use this field to upload your logo."),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate the subsites_look_form.  Save the logo if uploaded.
 */
function subsites_look_form_validate($form, &$form_state) {
  // Handle logo uploads.
  $validators = array('file_validate_is_image' => array());

  // Check for a new uploaded logo.
  $file = file_save_upload('logo_upload', $validators);
  if (isset($file)) {
    // File upload was attempted.
    if ($file) {
      // Put the temporary file in form_values so we can save it on submit.
      $form_state['values']['logo_upload'] = $file;
    }
    else {
      // File upload failed.
      form_set_error('logo_upload', t('The logo could not be uploaded.'));
    }
  }

  // If the user provided a path for a logo or favicon file, make sure a file
  // exists at that path.
  module_load_include('inc', 'system', 'system.admin');
  if ($form_state['values']['logo_path']) {
    $path = _system_theme_settings_validate_path($form_state['values']['logo_path']);
    if (!$path) {
      form_set_error('logo_path', t('The custom logo path is invalid.'));
    }
  }
}

/**
 * Submit handler for the subsites_look_form form.  Save the theme and css to apply when the subsite is active.
 */
function subsites_look_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  // If the user uploaded a new logo, save it to a permanent location
  // and use it in place of the default theme-provided file.
  if ($file = $values['logo_upload']) {
    unset($values['logo_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['default_logo'] = 0;
    $values['logo_path'] = $filename;
  }

  // If the user entered a path relative to the system files directory for
  // the logo, store a public:// URI so the theme system can handle it.
  module_load_include('inc', 'system', 'system.admin');
  if (!empty($values['logo_path'])) {
    $values['logo_path'] = _system_theme_settings_validate_path($values['logo_path']);
  }

  subsites_save(array(
    'sid' => $values['sid'],
    'css' => $values['css'],
    'theme' => $values['theme'],
    'default_logo' => $values['default_logo'],
    'logo_path' => $values['logo_path'],
  ));

  $form_state['redirect'] = 'admin/structure/subsites/' . $form_state['values']['sid'] . '/look';
  drupal_set_message(t('Subsite configuration changes saved.'));
}

/**
 * Menu callback, ask for confirmation of subsite deletion
 */
function subsites_delete_confirm_form($form, &$form_state, $subsite) {
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $subsite['sid'],
  );

  return confirm_form($form,
    t('Are you sure you want to delete %name?', array('%name' => $subsite['name'])),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/structure/subsites',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for executing subsite deletion after confirmation
 */
function subsites_delete_confirm_form_submit($form, &$form_state) {
  subsites_delete($form_state['values']['sid']);

  $form_state['redirect'] = 'admin/structure/subsites';
}
