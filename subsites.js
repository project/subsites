(function ($) {

Drupal.behaviors.vertical_tabs_subsiteFieldsetSummary = {
  attach: function (context) {
    $('fieldset#edit-subsites', context).drupalSetSummary(function (context) {
      var vals = [];

      // Block admin form
      if ($('input[type="checkbox"]:checked', context).length > 0) {
        $('input[type="checkbox"]:checked', context).each(function () {
          vals.push($.trim($(this).next('label').text()));
        });
        if (!vals.length) {
          vals.push(Drupal.t('Not restricted'));
        }
      }
      // Node form
      else if ($('select[name=subsite]', context).length > 0) {
        if ($('select[name=subsite]', context).val() == "-1") {
          return Drupal.t('None');
        }
        else {
          vals.push($.trim($('select[name=subsite] option:checked', context).text()));
        }
      }
      return vals.join(', ');
    });
  }
};

})(jQuery);