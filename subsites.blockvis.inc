<?php
/**
 * @file
 * Functions related to managing block visibility based on subsite.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 * Add an extra block visibility option to block add form.
 */
function subsites_form_block_add_block_form_alter(&$form, $form_state) {
  subsites_form_alter_block_visibility($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Add an extra block visibility option to block configure form.
 */
function subsites_form_block_admin_configure_alter(&$form, $form_state) {
  subsites_form_alter_block_visibility($form, $form_state);
}

/**
 * Shared function for block forms to alter them to include subsite block visibility settings.
 */
function subsites_form_alter_block_visibility(&$form) {
  drupal_add_js(drupal_get_path('module', 'subsites') . '/subsites.js');

  $default_setting = variable_get('subsites_block_' . $form['module']['#value'] . '_' . $form['delta']['#value']);
  $subsites = subsites_get_all();
  $options[SUBSITES_BLOCK_VISIBILITY_MAIN_SITE] = t('Main Site');
  foreach ($subsites as $subsite) {
    $options[$subsite['sid']] = check_plain($subsite['name']);
  }

  $form['visibility']['subsites'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subsites'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'visibility',
  );
  $form['visibility']['subsites']['is_subsite'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show block on particular subsites'),
    '#default_value' => $default_setting ? $default_setting : array(),
    '#options' => $options,
    '#description' => t('Show this block based on the current subsite.  Main site is active when all other subsites are not.'),
  );

  $form['#submit'][] = 'subsites_block_visibility_submit';
}

/**
 * Submit handler, handle extra block visibility option.
 */
function subsites_block_visibility_submit($form, &$form_state) {
  variable_set('subsites_block_' . $form_state['values']['module'] . '_' . $form_state['values']['delta'], $form_state['values']['is_subsite']);
}

/**
 * Implements hook_block_list_alter().
 * Removes blocks from view according to the current subsite.
 */
function subsites_block_list_alter(&$blocks) {
  $sid = subsites_get_current();

  foreach ($blocks as $key => $block) {
    $show_subsites =  variable_get('subsites_block_' . $block->module . '_' . $block->delta);

    // If we have a non-empty list of subsite restrictions and
    // the current page identifies as being part of a subsite and the currently active subsite is not in the list
    // for which this block should show, hide it.

    // Only need to take action if a list of subsite restrictions has been selected for this block.
    if (!empty($show_subsites)) {

      // If $sid is a truthy value, that means a subsite is active for the request, if that subsite
      // is not in the list of restrictions, hide the block.
      if ($sid) {
        if (!in_array($sid, $show_subsites)) {
          unset($blocks[$key]);
        }
      }
      // Here $sid is falsy, implying that the main site is active, check that the main site should show this block, if
      // not hide the block.
      else {
        if (!in_array(SUBSITES_BLOCK_VISIBILITY_MAIN_SITE, $show_subsites)) {
          unset($blocks[$key]);
        }
      }
    }
  }
}
