<?php

/**
 * Field handler to provide a list of subsites.
 */
class views_handler_field_node_subsites extends views_handler_field_prerender_list {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array(
      'table' => 'node',
      'field' => 'nid',
    );
  }

  function query() {
    $this->add_additional_fields();
    $this->field_alias = $this->aliases['nid'];
  }

  /**
   * Run before any fields are rendered.
   * @param $values
   */
  function pre_render($values) {
    $this->items = array();

    foreach ($values as $result) {
      $nids[] = $result->{$this->field_alias};
    }

    if (!empty($nids)) {
      $query = db_select('subsites', 's');
      $query->innerJoin('subsites_node', 'n', 's.sid = n.sid');
      $query
        ->fields('s', array('sid', 'name'))
        ->fields('n', array('nid'))
        ->condition('n.nid', $nids, 'IN');
      $result = $query->execute();
      foreach ($result as $subsite) {
        $this->items[$subsite->nid][$subsite->sid] = array(
          'subsite' => check_plain($subsite->name),
          'sid' => $subsite->sid,
        );
      }
    }
  }

  function render_item($count, $item) {
    return $item['subsite'];
  }

  function document_self_tokens(&$tokens) {
    $tokens['[' . $this->options['id'] . '-subsite' . ']'] = t('The name of the subsite.');
    $tokens['[' . $this->options['id'] . '-sid' . ']'] = t('The subsite ID of the subsite.');
  }

  function add_self_tokens(&$tokens, $item) {
    $tokens['[' . $this->options['id'] . '-subsite' . ']'] = $item['subsite'];
    $tokens['[' . $this->options['id'] . '-sid' . ']'] = $item['sid'];
  }
}
