<?php
/**
 * @file
 * Views module hooks and code.
 */

/**
 * Implements hook_views_data()
 */
function subsites_views_data() {
  $data = array();

  // Describe subsites table to views.
  $data['subsites']['table']['group']  = t('Subsite');

  // Advertise this table as a possible base table
  $data['subsites']['table']['base'] = array(
    'field' => 'sid',
    'title' => t('Subsites'),
    'help' => t('Subsites are a part of your website that can have its own menu, theme, custom CSS or anything else you want.'),
    'weight' => 50,
  );
  // Sid
  $data['subsites']['sid'] = array(
    'title' => t('Sid'),
    'help' => t('The subsite ID of the subsite.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // Name
  $data['subsites']['name'] = array(
    'title' => t('Name'),
    'help' => t('Name of the subsite'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // Weight
  $data['subsites']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('The subsite weight field'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // Status
  $data['subsites']['status'] = array(
    'title' => t('Enabled'),
    'help' => t('Whether or not the subsite is enabled'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Enabled'),
      'type' => 'yes-no',
    ),
  );

  // Tell views about the subsites_node table, path it back to the node base table.
  // These definitions are relevant only when Node (Content) is the base table.
  $data['subsites_node']['table']['group']  = t('Subsite');

  $data['subsites_node']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  // Sid, rendered as a list of subsites titles.
  $data['subsites_node']['sid'] = array(
    'title' => t('Subsite'),
    'help' => 'The subsite a node belongs to',
    'filter' => array(
      'handler' => 'views_handler_filter_node_subsites',
      'numeric' => TRUE,
    ),
    'field' => array(
      'handler' => 'views_handler_field_node_subsites',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_subsites',
    ),
  );

  $data['subsites_node']['current_subsite'] = array(
    'title' => t('Current subsite'),
    'help' => t('Get only nodes at the current subsite'),
    'filter' => array(
      'handler' => 'views_handler_filter_node_current_subsite',
    ),
  );

  return $data;
}
