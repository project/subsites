## Contents of this file

* Not ported to Drupal 7 yet.
* Introduction
* Recommended modules
* Installation
* Configuration
* Maintainers


## Not ported to Drupal 7 yet.

This submodule has not been ported to Drupal 7 yet.
Do not unable unless you want to help with porting.

## Introduction

The **Subsites Access Control** module XXXX

## Requirements

* [Advanced help hint][1]:<br>
  Required to link help text to online help and advanced help.

## Recommended modules

* [Advanced help][2]:<br>
  When this module is enabled, the module's `README.md` may be
  rendered in the browser.
* [Markdown filter][3]:<br>
  When this module is enabled, the module's `README.md` will be
  rendered with the markdown filter.

## Installation

Install and enable as you would normally install a contributed Drupal
module. See: [Installing modules][4] for further information.

## Configuration

[TBA]

## Maintainers

* [Davy Van Den Bremt](https://www.drupal.org/u/davyvdb) Original author
* [James Andres](https://www.drupal.org/user/33827) Drupal 7 upgrade
* [gisle](https://www.drupal.org/u/gisle) Current maintainer

Any help with development (patches, reviews, comments) are welcome.


[1]: https://www.drupal.org/project/advanced_help_hint
[2]: https://www.drupal.org/project/advanced_help
[3]: https://www.drupal.org/project/markdown
[4]: https://www.drupal.org/documentation/install/modules-themes/modules-7
