<?php

/**
 * Retrieve the subsite record for a certain subsite id.
 *
 * @param $sid
 *   The subsite id of the requested subsite.
 * @return
 *   The requested subsite.
 */
function subsites_get($sid) {
  if (!$sid) {
    return FALSE;
  }
  $subsite = db_query("SELECT * FROM {subsites} WHERE sid = :sid", array(':sid' => $sid))->fetchAssoc();
  $subsite['menu_name'] = db_query("SELECT menu_name FROM {subsites_menu} WHERE sid = :sid", array(':sid' => $sid))->fetchField();
  $path = subsites_get_css_path($sid);
  if (is_file($path)) {
    $subsite['css'] = file_get_contents($path);
  }
  else {
    $subsite['css'] = '';
  }

  return $subsite;
}

/**
 * Retrieve all subsites.
 *
 * @return
 *   An array of subsites.
 */
function subsites_get_all($only_active = FALSE) {
  $subsites = array();

  if ($only_active) {
    $result = db_query("SELECT * FROM {subsites} WHERE status = :status ORDER BY weight ASC", array(':status' => 1));
  }
  else {
    $result = db_query("SELECT * FROM {subsites} ORDER BY weight ASC");
  }

  foreach ($result as $subsite) {
    $subsites[] = (array)$subsite;
  }
  return $subsites;
}

/**
 * Save a subsite to the database.  $subsite should be a completely defined subsite.
 */
function subsites_save($subsite) {
  $is_new = FALSE;

  if (isset($subsite['sid'])) {
    $defaults = subsites_get($subsite['sid']);
  }
  else {
    $is_new = TRUE;

    $defaults = array(
      'pages' => NULL,
      'visibility' => SUBSITES_VISIBILITY_LISTED,
      'css_fid' => NULL,
      'theme' => -1,
      'weight' => 0,
      'status' => 0,
      'site_name' => NULL,
      'slogan' => NULL,
      'site_frontpage' => NULL,
      'default_logo' => 1,
      'logo_path' => NULL,
    );
  }

  $subsite = array_merge($defaults, $subsite);

  // Save css
  if (isset($subsite['css'])) {
    $file = file_save_data($subsite['css'], subsites_get_css_path($subsite['sid']), FILE_EXISTS_REPLACE);

    if ($file) {
      $subsite['css_fid'] = $file->fid;
    }
  }

  $primary_keys = $is_new ? array() : 'sid';
  drupal_write_record('subsites', $subsite, $primary_keys);

  if ($is_new) {
    // Create the menu
    $menu_name = _subsites_create_menu($subsite['sid'], $subsite['name']);

    // Insert into the subsites_menu table
    db_insert('subsites_menu')
      ->fields(array('sid' => $subsite['sid'], 'menu_name' => $menu_name))
      ->execute();

    module_invoke_all('subsites_add', $subsite);
  }
  else {
    module_invoke_all('subsites_edit', $subsite);
  }

  return $subsite['sid'];
}

/**
 * Delete an existing subsite.
 *
 * @param $sid
 *   Int; The subsite id of the subsite.
 */
function subsites_delete($sid) {
  $subsite = subsites_get($sid);

  // Delete css
  if (!empty($subsite['css_fid'])) {
    $file = file_load($subsite['css_fid']);
    if ($file) {
      file_delete($file);
    }
  }

  // Delete db entry
  db_delete('subsites')
    ->condition('sid', $sid)
    ->execute();

  module_invoke_all('subsites_delete', $subsite);
}

/**
 * Returns the currently active subsite.
 **/
function subsites_get_current() {
  static $sid;

  if (!isset($sid)) {
    $detection_methods = module_invoke_all('subsite_detection_methods');
    drupal_alter('subsite_detection_methods', $detection_methods);

    foreach ($detection_methods as $function => $function_info) {
      if (isset($function_info['file'])) {
        include_once DRUPAL_ROOT . '/' . $function_info['file'];
      }
      $sid = call_user_func($function);
      if ($sid) {
        break;
      }
    }
  }

  return $sid;
}

/**
 * Try to match a subsite to the current request using the pages lists defined for each
 * subsite.
 *
 * @return mixed
 *   sid of the active subsite, or FALSE if no active subsite found by pages.
 */
function _subsites_get_current_by_pages() {
  $subsites = subsites_get_all(TRUE);

  foreach ($subsites as $subsite) {
    if (_subsites_match_path($subsite['pages'], $subsite['visibility'])) {
      return $subsite['sid'];
    }
  }

  return FALSE;
}

/**
 * Detection method in which we try to place the current request in a subsite by menu
 *
 * @see menu_block.module menu_tree_rebuild()
 * @return mixed
 *   sid of the active subsite, or FALSE if no active subsite found by menu.
 */
function _subsites_get_current_by_menu() {
  $result = db_query("SELECT menu_name FROM {menu_links} WHERE link_path = :link_path", array(':link_path' => $_GET['q'] ? $_GET['q'] : '<front>'));

  if (module_exists('menu_block')) {
    // Menu block implements menu weighting, so that if a menu link if found in multiple menus, we can break the tie
    $menu_order = variable_get('menu_block_menu_order', array('main-menu' => '', 'user-menu' => ''));

    foreach ($result as $row) {
      if (!isset($first)) {
        $active_menu_name = $row->menu_name;
        $first = TRUE;
      }

      // Check if the menu is in the list of available menus.
      if (isset($menu_order[$row->menu_name])) {
        // Mark the menu.
        $menu_order[$row->menu_name] = MENU_TREE__CURRENT_PAGE_MENU;
      }
    }

    // Find the first marked menu.
    if (array_search(MENU_TREE__CURRENT_PAGE_MENU, $menu_order)) {
      $active_menu_name = array_search(MENU_TREE__CURRENT_PAGE_MENU, $menu_order);
    }
  }
  else {
    $active_menu_name = $result->fetchField();
  }

  $subsites = subsites_get_all(TRUE);

  if (isset($active_menu_name)) {
    foreach ($subsites as $subsite) {
      $menus = db_query('SELECT menu_name FROM {subsites_menu} WHERE sid = :sid', array(':sid' => $subsite['sid']))->fetchAllKeyed(0, 0);

      foreach($menus as $menu) {
        if ($menu == $active_menu_name) {
          return $subsite['sid'];
        }
      }
    }
  }

  return FALSE;
}

/**
 * Subsite dertection method that uses the active node to attempt to find the active subsite.
 *
 * @return mixed
 *   sid of the active subsite, or FALSE if no active subsite found by node.
 */
function _subsites_get_current_by_node() {
  $node = menu_get_object();

  if ($node && $node->subsite) {
    $subsite = subsites_load($node->subsite);
    if ($subsite['status']) {
      return $node->subsite;
    }
  }

  return FALSE;
}

/**
 * Get a list of subsite options to use in select fields, checkboxes, ....
 *
 * @return
 *   Array; Keys are subsite ids. Values are subsite names.
 */
function _subsites_select_options($add_default = TRUE) {
  $options = array();

  if ($add_default) {
    $options[-1] = t('<None>');
  }

  $subsites = subsites_get_all();
  foreach ($subsites as $subsite) {
    $options[$subsite['sid']] = $subsite['name'];
  }
  return $options;
}

/**
 * Determine if we should activate the subsite given the current path, and the visibility options.
 *
 * @param $pages
 *  List of path patterns to activate/de-active subsite for
 * @param $visibility
 *  Flag to indicate which matching option to use.
 */
function _subsites_match_path($pages, $visibility) {

  if ($visibility < SUBSITES_VISIBILITY_PHP) {
    // Convert path to lowercase. This allows comparison of the same path
    // with different case. Ex: /Page, /page, /PAGE.
    $pages = drupal_strtolower($pages);
    $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
    // Compare with the internal and path alias (if any).
    $page_match = drupal_match_path($path, $pages);
    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
    }
    // When $visibility has a value of 0, the subsite is active on
    // all pages except those listed in $pages. When set to 1, it
    // is active only on those pages listed in $pages.
    $page_match = !($visibility xor $page_match);
  }
  else {
    $page_match = FALSE;
    if (module_exists('php')) {
      if (php_eval($pages)) {
        $page_match = TRUE;
      }
    }
  }

  return $page_match;
}

/**
 * Create a menu for a subsite.
 *
 * @param int $sid
 *   The subsite id to create a menu for.
 * @param string $name
 *   The name of the subsite, used in creating a machine name of the menu.
 * @return string
 *   Machine name of the menu created.
 */
function _subsites_create_menu($sid, $name) {
  $path = 'admin/structure/menu/manage/';

  // Add 'menu-' to the menu name to help avoid name-space conflicts.
  $menu['menu_name'] = truncate_utf8(sprintf('subsites-%d-%s', $sid, _subsites_slug($name)), 32); // Maximum menu_name length is 32
  $menu['title'] = check_plain($name);
  $menu['description'] = t('Auto-created menu for the @name subsite.', array('@name' => $name));
  $link['link_title'] = $name;
  $link['link_path'] = $path . $menu['menu_name'];
  $link['router_path'] = $path . '%';
  $link['module'] = 'menu';
  $link['plid'] = db_query("SELECT mlid FROM {menu_links} WHERE link_path = :link AND module = :module", array(
    ':link' => 'admin/structure/menu',
    ':module' => 'system'
  ))
  ->fetchField();

  menu_link_save($link);
  menu_save($menu);

  return $menu['menu_name'];
}

/**
 * Calculate a slug with a maximum length for a string.   Used to create a machine name
 * for the menu created.  Also used for tokens and template files.
 *
 * @param $string
 *   The string you want to calculate a slug for.
 * @param $length
 *   The maximum length the slug can have.
 * @return
 *   A string representing the slug
 */
function _subsites_slug($string, $length = 50, $separator = '-') {
  // Transliterate
  $string = _subsites_transliterate($string);

  // Lowercase
  $string = strtolower($string);

  // Replace non alphanumeric and non underscore charachters by separator
  $string = preg_replace('/[^a-z0-9]/i', $separator, $string);

  // Replace multiple occurences of separator by one instance
  $string = preg_replace('/' . preg_quote($separator) . '[' . preg_quote($separator) . ']*/', $separator, $string);

  // Cut off to maximum length
  if ($length > -1 && strlen($string) > $length) {
    $string = substr($string, 0, $length);
  }

  // Remove separator from start and end of string
  $string = preg_replace('/' . preg_quote($separator) . '$/', '', $string);
  $string = preg_replace('/^' . preg_quote($separator) . '/', '', $string);

  return $string;
}

/**
 * Transliterate a given string.  Used by _subsites_slug().
 *
 * @see _subsites_slug().
 * @param $string
 *   The string you want to transliterate.
 * @return
 *   A string representing the transliterated version of the input string.
 */
function _subsites_transliterate($string) {
  static $charmap;
  if (!$charmap) {
    $charmap = array(
      // Decompositions for Latin-1 Supplement
      chr(195) . chr(128) => 'A', chr(195) . chr(129) => 'A',
      chr(195) . chr(130) => 'A', chr(195) . chr(131) => 'A',
      chr(195) . chr(132) => 'A', chr(195) . chr(133) => 'A',
      chr(195) . chr(135) => 'C', chr(195) . chr(136) => 'E',
      chr(195) . chr(137) => 'E', chr(195) . chr(138) => 'E',
      chr(195) . chr(139) => 'E', chr(195) . chr(140) => 'I',
      chr(195) . chr(141) => 'I', chr(195) . chr(142) => 'I',
      chr(195) . chr(143) => 'I', chr(195) . chr(145) => 'N',
      chr(195) . chr(146) => 'O', chr(195) . chr(147) => 'O',
      chr(195) . chr(148) => 'O', chr(195) . chr(149) => 'O',
      chr(195) . chr(150) => 'O', chr(195) . chr(153) => 'U',
      chr(195) . chr(154) => 'U', chr(195) . chr(155) => 'U',
      chr(195) . chr(156) => 'U', chr(195) . chr(157) => 'Y',
      chr(195) . chr(159) => 's', chr(195) . chr(160) => 'a',
      chr(195) . chr(161) => 'a', chr(195) . chr(162) => 'a',
      chr(195) . chr(163) => 'a', chr(195) . chr(164) => 'a',
      chr(195) . chr(165) => 'a', chr(195) . chr(167) => 'c',
      chr(195) . chr(168) => 'e', chr(195) . chr(169) => 'e',
      chr(195) . chr(170) => 'e', chr(195) . chr(171) => 'e',
      chr(195) . chr(172) => 'i', chr(195) . chr(173) => 'i',
      chr(195) . chr(174) => 'i', chr(195) . chr(175) => 'i',
      chr(195) . chr(177) => 'n', chr(195) . chr(178) => 'o',
      chr(195) . chr(179) => 'o', chr(195) . chr(180) => 'o',
      chr(195) . chr(181) => 'o', chr(195) . chr(182) => 'o',
      chr(195) . chr(182) => 'o', chr(195) . chr(185) => 'u',
      chr(195) . chr(186) => 'u', chr(195) . chr(187) => 'u',
      chr(195) . chr(188) => 'u', chr(195) . chr(189) => 'y',
      chr(195) . chr(191) => 'y',
      // Decompositions for Latin Extended-A
      chr(196) . chr(128) => 'A', chr(196) . chr(129) => 'a',
      chr(196) . chr(130) => 'A', chr(196) . chr(131) => 'a',
      chr(196) . chr(132) => 'A', chr(196) . chr(133) => 'a',
      chr(196) . chr(134) => 'C', chr(196) . chr(135) => 'c',
      chr(196) . chr(136) => 'C', chr(196) . chr(137) => 'c',
      chr(196) . chr(138) => 'C', chr(196) . chr(139) => 'c',
      chr(196) . chr(140) => 'C', chr(196) . chr(141) => 'c',
      chr(196) . chr(142) => 'D', chr(196) . chr(143) => 'd',
      chr(196) . chr(144) => 'D', chr(196) . chr(145) => 'd',
      chr(196) . chr(146) => 'E', chr(196) . chr(147) => 'e',
      chr(196) . chr(148) => 'E', chr(196) . chr(149) => 'e',
      chr(196) . chr(150) => 'E', chr(196) . chr(151) => 'e',
      chr(196) . chr(152) => 'E', chr(196) . chr(153) => 'e',
      chr(196) . chr(154) => 'E', chr(196) . chr(155) => 'e',
      chr(196) . chr(156) => 'G', chr(196) . chr(157) => 'g',
      chr(196) . chr(158) => 'G', chr(196) . chr(159) => 'g',
      chr(196) . chr(160) => 'G', chr(196) . chr(161) => 'g',
      chr(196) . chr(162) => 'G', chr(196) . chr(163) => 'g',
      chr(196) . chr(164) => 'H', chr(196) . chr(165) => 'h',
      chr(196) . chr(166) => 'H', chr(196) . chr(167) => 'h',
      chr(196) . chr(168) => 'I', chr(196) . chr(169) => 'i',
      chr(196) . chr(170) => 'I', chr(196) . chr(171) => 'i',
      chr(196) . chr(172) => 'I', chr(196) . chr(173) => 'i',
      chr(196) . chr(174) => 'I', chr(196) . chr(175) => 'i',
      chr(196) . chr(176) => 'I', chr(196) . chr(177) => 'i',
      chr(196) . chr(178) => 'IJ', chr(196) . chr(179) => 'ij',
      chr(196) . chr(180) => 'J', chr(196) . chr(181) => 'j',
      chr(196) . chr(182) => 'K', chr(196) . chr(183) => 'k',
      chr(196) . chr(184) => 'k', chr(196) . chr(185) => 'L',
      chr(196) . chr(186) => 'l', chr(196) . chr(187) => 'L',
      chr(196) . chr(188) => 'l', chr(196) . chr(189) => 'L',
      chr(196) . chr(190) => 'l', chr(196) . chr(191) => 'L',
      chr(197) . chr(128) => 'l', chr(197) . chr(129) => 'L',
      chr(197) . chr(130) => 'l', chr(197) . chr(131) => 'N',
      chr(197) . chr(132) => 'n', chr(197) . chr(133) => 'N',
      chr(197) . chr(134) => 'n', chr(197) . chr(135) => 'N',
      chr(197) . chr(136) => 'n', chr(197) . chr(137) => 'N',
      chr(197) . chr(138) => 'n', chr(197) . chr(139) => 'N',
      chr(197) . chr(140) => 'O', chr(197) . chr(141) => 'o',
      chr(197) . chr(142) => 'O', chr(197) . chr(143) => 'o',
      chr(197) . chr(144) => 'O', chr(197) . chr(145) => 'o',
      chr(197) . chr(146) => 'OE', chr(197) . chr(147) => 'oe',
      chr(197) . chr(148) => 'R', chr(197) . chr(149) => 'r',
      chr(197) . chr(150) => 'R', chr(197) . chr(151) => 'r',
      chr(197) . chr(152) => 'R', chr(197) . chr(153) => 'r',
      chr(197) . chr(154) => 'S', chr(197) . chr(155) => 's',
      chr(197) . chr(156) => 'S', chr(197) . chr(157) => 's',
      chr(197) . chr(158) => 'S', chr(197) . chr(159) => 's',
      chr(197) . chr(160) => 'S', chr(197) . chr(161) => 's',
      chr(197) . chr(162) => 'T', chr(197) . chr(163) => 't',
      chr(197) . chr(164) => 'T', chr(197) . chr(165) => 't',
      chr(197) . chr(166) => 'T', chr(197) . chr(167) => 't',
      chr(197) . chr(168) => 'U', chr(197) . chr(169) => 'u',
      chr(197) . chr(170) => 'U', chr(197) . chr(171) => 'u',
      chr(197) . chr(172) => 'U', chr(197) . chr(173) => 'u',
      chr(197) . chr(174) => 'U', chr(197) . chr(175) => 'u',
      chr(197) . chr(176) => 'U', chr(197) . chr(177) => 'u',
      chr(197) . chr(178) => 'U', chr(197) . chr(179) => 'u',
      chr(197) . chr(180) => 'W', chr(197) . chr(181) => 'w',
      chr(197) . chr(182) => 'Y', chr(197) . chr(183) => 'y',
      chr(197) . chr(184) => 'Y', chr(197) . chr(185) => 'Z',
      chr(197) . chr(186) => 'z', chr(197) . chr(187) => 'Z',
      chr(197) . chr(188) => 'z', chr(197) . chr(189) => 'Z',
      chr(197) . chr(190) => 'z', chr(197) . chr(191) => 's',
      // Euro Sign
      chr(226) . chr(130) . chr(172) => 'E'
    );
  }

  // transliterate
  return strtr($string, $charmap);
}

/**
 * Loads the options for the themes select form element.
 */
function _subsites_theme_options($add_default = TRUE) {
  $options = array();

  if ($add_default) {
    $options[-1] = '<' . t('System default') . '>';
  }

  foreach (list_themes() as $theme) {
    $options[$theme->name] = t('@name (@status)', array('@name' => $theme->name, '@status' => ($theme->status ? t('Enabled') : t('Disabled'))));
  }

  return $options;
}

/**
 * Returns the CSS path for a subsite.
 */
function subsites_get_css_path($sid) {
  $path = 'public://subsites';
  file_prepare_directory($path, FILE_CREATE_DIRECTORY);
  return $path . '/subsites-' . $sid . '.css';
}
