## Contents of this file

* Introduction
* Recommended modules
* Installation
* Configuration
* Maintainers


## Introduction

The **Subsites** module let you create *subsites*, which are parts of
your website that can have its own menu, theme, and custom CSS.

Subsites can be defined in a few manners:

* Marking individual nodes as belonging to a subsite.
* Defining all pages listed in the subsite menu as belonging to a
  subsite.
* Defining paths that belong to a subsite. This is done in the same
  way block visibility works. So you can use PHP too!

A subsites can have:

* their own page template: `page-subsites-{subsite-id}` or
  `page-subsites-{subsite-slug-name}.tpl.php`;
* their own CSS file;
* their own theme, different from the default theme;
* their own menu.

Subsites can be:

* a condition as defined by the in [Context module][4];
* used to define block visibility.

Please note that each path/node can only belong to a single
subsite. If multiple subsites' conditions apply for the path, the
subsite with the lowest weight is the active one.

Themers and module writers can react differently for each subsite
using `subsites_get_current` and `subsites_get`.


## Requirements

* [Advanced help hint][1]:<br>
  Required to link help text to online help and advanced help.

## Recommended modules

* [Advanced help][2]:<br>
  When this module is enabled, the module's `README.md` may be
  rendered in the browser.
* [Markdown filter][3]:<br>
  When this module is enabled, the module's `README.md` will be
  rendered with the markdown filter.
* [Context module][4]:<br>
  This module allow you to use a subsite as a condition.


## Installation

Install and enable as you would normally install a contributed Drupal
module. See: [Installing modules][5] for further information.

## Configuration

* Go to *Admininster » Structure » Subsites*.
* Click the *Add subsite* tab.

* Fill in details (Name, Weight and Enabled). After saving the subsite
  will be created and a companion menu will exist. To view this menu,
  navigate to *Administer » Structure » Menus*).

* You can now :

  - Reconfigure the subsite by choosing "Edit" in the subsites
    overview page (*Admininster » Structure » Subsites*). There should
    now be a tab, *Content*, to add specific nodes to the subsite, and
    *Look and feel* to add CSS and a theme to the subsite.

  - Add nodes, views, … to the subsite menu.

  - A vertical tab (*Subsite*) is added to a nodes edit form.  You can
    use this to mark nodes as belonging to a certain subsite.

  - Put the "Subsite menu" block in some region using the block admin on
    *Administer » Structure » Blocks*.

  - Restrict visibility for some blocks to only show up in specific subsites.
  

## Alternatives

For alternative approches to segment a site into "parts" that are
styled differently, check out:

* [Context][4]
* [ThemeKey][6]
* [Sections][7] - D6 only

## Maintainers

* [Davy Van Den Bremt](https://www.drupal.org/u/davyvdb) Original author
* [James Andres](https://www.drupal.org/user/33827) Drupal 7 upgrade
* [gisle](https://www.drupal.org/u/gisle) Current maintainer

Any help with development (patches, reviews, comments) are welcome.


[1]: https://www.drupal.org/project/advanced_help_hint
[2]: https://www.drupal.org/project/advanced_help
[3]: https://www.drupal.org/project/markdown
[4]: https://www.drupal.org/project/context
[5]: https://www.drupal.org/documentation/install/modules-themes/modules-7
[6]: https://www.drupal.org/project/themekey
[7]: https://www.drupal.org/project/sections
